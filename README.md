gitlab page https://gitlab.com/userAA/test-task-gitlab-english-comments.git
gitlab comment test-task-gitlab-english-comments

project test-task (showing test task)
technologies used (only frontend and ready-made json-server here)
    axios,
    bootstrap,
    bootstrap-icons,
    react,
    react-dom,
    react-scripts; 
Development React-application for conforming the table with data.
Functional.
Sorting according to columns: at clicking on name of column, rows of table is being sorted according to ascending, at repeated click -
according to descending. The direction of sorting is being indicated by graphic elements or text message.
Client pagination: the data must be displayed page by page, maximum twenty elements on page. Necessary to represent user 
navigation for switching between pages.
Filtration: component is text field, in which user enters text. Filtration implements at clicking on button "Search".
At click on page of table the values of fields are outputing in additional block under table.
Data in table are being downloaded from server. Method of downloading from server is any.
Above table is button "show add contact form", according to clicking on which the form of adding set appears.
After filing of all input fields the button "add contact" activates which inserts new contact in kind of filled row in block of table
and on server.
Above table is button "show delete contact form", according to clicking on which appears the form of removing contact.
After indicating of ID removing contact, the button "delete contact" activates which removes removing contact from 
table block and from server.
For demonstration of component work need to make up simple HTML page. The user is asked to select a data set: small or big.
At selection of set of data it uploads from server and according to data the table is being created.