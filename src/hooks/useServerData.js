import { useState, useEffect } from "react"
import infoApi from '../core-api/api'

//homemade hook of receiving data according to contacts from server according to address url
const useServerData = ({url, isRequestOnServer, setIsLoading}) => {
  //here will collect received data from contacts
  const [contactData, setContactData] = useState([]);

  useEffect(() => {
    //we do not make any requests if flag isRequestOnServer  negative
    if (!isRequestOnServer)
    {
      return;
    }
    //making a request to the server at address url for receiving information about all contacts
    //on web pages according to this address
    infoApi.getAll(url).then((res) => 
    {
       //fix of receiving data
      setContactData(res.data); 
      //flag of downloading information with server we making negative
       setIsLoading(false);      
    })
  }, [url, isRequestOnServer, setIsLoading])

  //returning result
  return [{contactData, setContactData}];
}

export default useServerData;