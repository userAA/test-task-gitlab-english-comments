import React, {useState} from 'react';
import useServerData from './hooks/useServerData';
import Switcher from './switcher/switcher';
import Home from './home/home';

function App() {
  //the flag of activation request into server for downloading information
  //isRequestOnServer == true - indicated request into server is activated overwise no
  const [isRequestOnServer, setIsRequestOnServer] = useState(false);
  //address of storage information about contacts on server
  const [url, setUrl] = useState("");
  //the flag of loading information from server
  const [isLoading, setIsLoading]     = useState(false);
  
  //pressing function or button of receiving small amount of contacts or button of receiving big amount of contacts
  const buttonHandler = (url) => {
    //address of storage information about contacts on server is defined
    setUrl(url);  
    //the flag of activation request into server about downloading information positive            
    setIsRequestOnServer(true);
    //flag of loading information from server positive
    setIsLoading(true);         
  }

  //receiving data contactData from server according to address url using homemade hook useServerData
  const [{contactData, setContactData}] = useServerData({url,setUrl, isRequestOnServer, setIsLoading});

  return (
    <div className="container">
      {/*a block of buttons for making requests to download information on contacts from the server
        at initial time moment all buttons with disabled === false */}
      <Switcher buttonHandler={buttonHandler}/> 
      <div style={{ margin: "25px 0 0 0"}}>
        {/*Main block of project */}  
        <Home
          url = {url}
          contactData = {contactData}
          setContactData = {setContactData}
          isLoading = {isLoading}  
          setIsRequestOnServer = {setIsRequestOnServer}
          isRequestOnServer = {isRequestOnServer}
        />
      </div>
    </div>
  );
}

export default App;