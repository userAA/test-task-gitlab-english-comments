import React, { Fragment, useState, useEffect } from 'react'
import Table from '../table/table';
import Loader from '../loader/loader';
import AddContactForm from "../actionContacts/addContactForm";
import DeleteContactForm from "../actionContacts/deleteContactForm";
import Paginator from '../paginator/paginator';
import infoApi from '../core-api/api';

//main blok project
const Home = ({
    //address on server according to which information about contacts was downloaded
    url,                   
    //the set of contacts downloaded from the server
    contactData,
    setContactData,
    //flag of loading contacts process
    isLoading,
    setIsRequestOnServer,
    //the flag of activation request for loading contacts with server according to address url
    isRequestOnServer
}) => {

    //limited number of contacts in table
    const limitCountContactsPage = 20;
    //filtered contacts
    let filteredData = [];
    //the contacts in table, which correspond to current page of block table 
    let currentBlockRows = {};

    //the search label of necessary contacts from all contacts in block of table
    const [searchText, setSearchText] = useState('');
    //the number of pages in block of table       
    const [totalCountPage, setTotalCountPage] = useState(0);
    //the number of current page of block table    
    const [currentPageNumber, setCurrentPageNumber] = useState(1);

    useEffect(() => {
        //new request for downloading information from server was carried out
        if (isRequestOnServer) {
            //the label search of needed contacts according to all contacts in block of table we equaling to ''
            setSearchText('');
            //the number of pages in block table we equaling to 0
            setTotalCountPage(0);
            //the number of current page of block table we equaling to 1
            setCurrentPageNumber(1);
        }
    },
        [
            isRequestOnServer,
            setCurrentPageNumber,
            setTotalCountPage
        ])

    //the adding function of new project in block of table and into server
    const addContactData = ({ id, firstName, lastName, email, phone }) => {
        infoApi.addRow(url, id, firstName, lastName, email, phone).then(() => {
            contactData.push({ id, firstName, lastName, email, phone });
        })
    }

    //the removing function of available contact from block table and from server
    const deleteContactData = (id) => {
        infoApi.deleteRow(url, id).then(() => {
            let newConcatData = contactData.filter(el =>  el.id !== id);
            setContactData(newConcatData);
        })
    }

    //the defining function of set contacts corresponding to current page of block tables from array filteredData
    const createCurrentBlockRows = (pg) => {
        let lastBlockRow = 0;

        if (totalCountPage > 0 && totalCountPage < pg) {
            //the case when filtration is applied here it is possible
            lastBlockRow = totalCountPage * limitCountContactsPage;
        }
        else 
        {
            //all other cases, when filtering according to label searchText for all downloaded contacts are not applied
            lastBlockRow = pg * limitCountContactsPage;
        }
        const firstBlockRow = lastBlockRow - limitCountContactsPage;
        currentBlockRows = filteredData.slice(firstBlockRow, lastBlockRow);
    }

    //carrying out filtration entire all downloaded contacts for block of table according to fixed label searchText
    const getFilteredData = () => {
        //at searchText === '' we do not filter
        if (searchText === '') {
            filteredData = contactData;
        }
        else {
            //in other cases we do filter
            filteredData = contactData.filter(el => {
                return (
                    el['firstName'].toLowerCase().includes(searchText.toLowerCase())
                    || el['lastName'].toLowerCase().includes(searchText.toLowerCase())
                    || el['email'].toLowerCase().includes(searchText.toLowerCase())
                    || el['phone'].toLowerCase().includes(searchText.toLowerCase())
                );
            })
        }
        createCurrentBlockRows(currentPageNumber);
    }

    //the function of fixing an element searchText by which we seeking needed contacts from all uploaded contacts
    const onSearchSend = (text) => {
        //fix label searchText by which we seeking needed contacts from all uploaded contacts
        setSearchText(text);
    }

    //carrying out filtering entire all downloaded contacts in table according to fixed label searchText
    getFilteredData();

    useEffect(() => {
        if (isLoading) {
            return;
        }
        //Define the number of pages of block table when are all the contacts already with server is loaded
        //and filtering of uploaded contacts according to label searchText is carried out
        setTotalCountPage(Math.ceil(filteredData.length / limitCountContactsPage));

    }, [isLoading, filteredData.length, setTotalCountPage])

    return (
        isLoading ?
            //contact loading alarm, turns on and remains as long as the download is in progress
            <Loader /> :
            currentBlockRows.length ?
                (
                    <div>
                        <Fragment>
                            {/*the adding form of new contact in table and into server */}
                            <AddContactForm addContactData={addContactData} />
                            {/*the removing form of available contact from table and from server */}
                            <DeleteContactForm deleteContactData={deleteContactData} />
                            {/*the table of data according to current page */}
                            <Table
                                currentBlockRows={currentBlockRows}
                                contactData={contactData}
                                setContactData={setContactData}
                                onSearchSend={onSearchSend}
                                isRequestOnServer={isRequestOnServer}

                            />

                        </Fragment>
                        {
                            !isLoading && filteredData.length > limitCountContactsPage &&
                            //Paginator of pages
                            <Paginator
                                totalCountPage={totalCountPage}
                                createCurrentBlockRows={createCurrentBlockRows}
                                currentPageNumber={currentPageNumber}
                                setCurrentPageNumber={setCurrentPageNumber}
                                setIsRequestOnServer={setIsRequestOnServer}
                                isRequestOnServer={isRequestOnServer}
                            />
                        }
                    </div>
                ) :
                (
                    <></>
                )
    )
}

export default Home;