import React from 'react'

//separate contact information
const SeparateContact = ({separateContactData}) => {
    return (
        <div>
            {/*contact ID*/}
            <div>
                id: <b>{separateContactData.id}</b>
            </div>
            {/*contact firstName*/}
            <div>
                firstName: <b>{separateContactData.firstName}</b>
            </div>
            {/*contact lastName*/}
            <div>
                lastName: <b>{separateContactData.lastName}</b>
            </div>
            {/*contact phone*/}
            <div>
                phone: <b>{separateContactData.phone}</b>
            </div>
            {/*contact email*/}
            <div>
                email: <b>{separateContactData.email}</b>
            </div>
        </div>
    )
}

export default SeparateContact;