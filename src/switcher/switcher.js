import React, {useState} from 'react'

//the block of buttons for implementing of requests for downloading information according to contacts from server
//at initial time moment all buttons with disabled === false
const Switcher = ({buttonHandler}) => {
    // a small number of contacts are stored on the server in json format at this address
    //const smallUrl = "http://localhost:3001/contactsSmall";
    const smallUrl = "https://test-task-deploy-server.herokuapp.com/contactsSmall";
    // a big number of contacts are stored on the server in json format at this address
    //const bigUrl = "http://localhost:3001/contactsBig";
    const bigUrl = "https://test-task-deploy-server.herokuapp.com/contactsBig";

    //web address according to which from server we downloading data about contacts 
    const [outputUrl, setOutputUrl] = useState('');

    const defineUrl = (url) => {
        //defining web address according to which from server we downloading data about contacts
        setOutputUrl(url);
        //implementing request for downloading information about contacts from server according to defined web address
        buttonHandler(url); 
    }

    return (
        <div style={{display: 'flex', justifyContent: "center", margin: "25px 0 0 0"}}>
            {/*the button for request implementation according to downloading a little of information about contacts,
                when making a request, this button becomes disabled = true and the button
                named Big becomes disabled = false */}
            <button 
                className='btn btn-danger' 
                disabled= {(outputUrl !== smallUrl)  ? false : true} 
                style={{margin: "0 25px 0 0"}} 
                onClick={() => {defineUrl(smallUrl)}}
            >
                Small
            </button>
            {/*the button for request implementation according to downloading a big of information about contacts,
                when making a request, this button becomes disabled = true and the button
                named Small becomes disabled = false */}
            <button 
                className='btn btn-warning' 
                disabled= {(outputUrl !== bigUrl)  ? false : true} 
                onClick={() => {defineUrl(bigUrl)}}
            >
                Big
            </button>
        </div>
    )
}

export default Switcher;