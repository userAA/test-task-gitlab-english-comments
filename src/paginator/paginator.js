import React, {useState, useEffect} from "react";

//pagination block
const Paginator = ({
    //quantity of pages
    totalCountPage, 
    createCurrentBlockRows,
    //the number of current page
    currentPageNumber, 
    setCurrentPageNumber,
    setIsRequestOnServer,
    //the flag of activate request for downloading information with server
    isRequestOnServer 
}) => {

    //making up of array pages
    let pages = [];
    for (let i=1; i <= totalCountPage; i++) {
      pages.push(i);
    }

    //the content of button with name Next
    const [buttonNextDisabled, setButtonNextDisabled] = useState('');
    //the content of button with name Previous
    const [buttonPreviousDisabled, setButtonPreviousDisabled] = useState('');

    useEffect(() => {
        if (isRequestOnServer)
        {
            //at positive flag of request for downloading information from server the button with name Next is activated
            setButtonNextDisabled('');
            //if the request flag for downloading information from server is negative 
            //then the button with name Previous is deactivated
            setButtonPreviousDisabled('disabled');
            //the flag of activation request for downloading information with server we making negative
            setIsRequestOnServer(false); 
        }
    },
    [
        isRequestOnServer, 
        setCurrentPageNumber,
        setButtonNextDisabled,
        setButtonPreviousDisabled,
        setIsRequestOnServer
    ])

    const currentPage = (pg) => {
        //setting new current page
        setCurrentPageNumber(pg);
    
        //activating the button with name Next
        if (buttonNextDisabled === 'disabled') 
        {
            setButtonNextDisabled('');
        }
    
        //activating the button with name Previous
        if (buttonPreviousDisabled === 'disabled') 
        {
            setButtonPreviousDisabled('');
        }

        //creating block of new contacts, corresponding to the page with the number pg
        createCurrentBlockRows(pg);
    }

    //the function of switching to the previous page
    const onPreviousClick = () => {
        if (currentPageNumber > 1)
        {
            //changing current page if possible
            setCurrentPageNumber(currentPageNumber-1);
        }
        else
        {
            //otherwise deactivating the button with name Previous
            setButtonPreviousDisabled('disabled');
        }
    }

    //the transit function on next page
    const onNextClick = () => {
        if (currentPageNumber < pages.length)
        {
            //changing current page if possible
            setCurrentPageNumber(currentPageNumber+1);
        }
        else
        {
            //otherwise deactivate button with name Next
            setButtonNextDisabled('disabled');
        }
    }

    return (
        <nav aria-label="...">
            <ul className="pagination">
                {/*the button of switching to the previous page */}
                <li className={`page-item ${buttonPreviousDisabled}`}>
                    <a className="page-link" href="/#" tabIndex="-1" onClick={() => {onPreviousClick()}}>
                        Previous
                    </a>
                </li> 
                {/*showing all pages and we selecting a fixed page with a number currentPageNumber */}
                {pages.map((p) => {
                    return (
                        <li className={
                            ((currentPageNumber === p) || (totalCountPage === p && totalCountPage < currentPageNumber))  ? `page-item active` : `page-item`} key={p}
                        >
                            <a className="page-link" href="/#" onClick={() => {currentPage(p)}}>
                                {p} 
                            </a>
                        </li>
                    )
                })}
                {/*the button to go on next page */}
                <li className={`page-item ${buttonNextDisabled}`}>
                    <a className="page-link" href="/#" onClick={() => {onNextClick()}}>
                        Next
                    </a>
                </li>
            </ul>
        </nav>
    )
}

export default Paginator;