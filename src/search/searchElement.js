import React, {useState} from 'react'

//the block of the element assignment by which the necessary contacts are found from all downloaded contacts
const SearchElement = ({
    //the function of fixing the element by which we are looking for the necessary contacts from all downloaded contacts
    onSearchSend 
}) => {
    //the searchValue element according to which the necessary contacts are searched from all downloaded contacts
    const [searchValue, setSearchValue] = useState('');

    return (
        <div 
            className="input-group mb-3 mt-3" 
            style={{display: 'flex', justifyContent: "center", margin: "25px 0 0 0"}}
        >
            {/*the window for specifying the element by which the necessary contacts are found from all downloaded contacts */}
            <input
                type="text"
                className="form-control"
                style={{margin: "0 25px 0 0"}}
                placeholder="Recipient's username"
                aria-label="Rcipient's username"
                aria-describedby="basic-addon2"

                value={searchValue}
                onChange={(event) => {setSearchValue(event.target.value)}}
            />
            <div className="input-group-append">
                {/*the button for fixing the elements by which the necessary contacts are found from all downloaded contacts*/}
                <button
                    className="btn btn-outline-secondary"
                    type="button"
                    onClick={
                        () => { 
                            //we fixing element on which we searching needed contacts from all downloaded contacts 
                            onSearchSend(searchValue); 
                            //the element by which the necessary contacts from all downloaded contacts are assigned to ''
                            setSearchValue('');
                        }
                    }
                >
                    Search
                </button>
            </div>
        </div>
    )
}

export default SearchElement;