import axios from 'axios'

//query structure
const infoApi = {
    //request for receiving all contacts from server according to specified address url
    getAll: (url) => {
        return axios.get(url)
    },
    //request for adding new project into server according to specified address url
    addRow: (url, id, firstName, lastName, email, phone) => {
        return axios.post(url, {
            id: id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone
        })
    },
    //request for removing contact with ID id from server according to specified address url
    deleteRow: (url,id) => {
        return axios.delete(url+'/'+id)
    }
}

export default infoApi;