import React from 'react';
import './loader.css'

//Alarm of downloading information from the server
const Loader = () => {
    return (
        <div className='lds-dual-ring'></div>
    )
}

export default Loader;