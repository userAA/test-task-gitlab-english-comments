import React, { useState } from 'react';

//the form of adding new contact in table and into server
const AddContactForm = ({
    //the function of adding new contact in table and into server, if total information needed for this is known
    addContactData 
}) => {
    //flag of opening form
    const [isFormOpen, setIsFormOpen] = useState(false);
    //ID of adding contact
    const [id, setId]                 = useState('');  
    //firstName of adding contact  
    const [firstName, setFirstName]   = useState('');  
    //lastName of adding contact
    const [lastName, setLastName]     = useState('');   
    //email of adding contact 
    const [email, setEmail]           = useState('');   
    //phone of adding contact
    const [phone, setPhone]           = useState('');   

    //the function, associated with button of adding new contact
    const submitHandler = () => {
        if (id !== '' && firstName !== '' && lastName !== '' &&  email !== '' && phone !== '')
        {
            //adding new contact in table and into server, if total information, needed for this is known
            addContactData({id, firstName, lastName, email, phone});
            setId('');  
            setFirstName('');  
            setLastName('');  
            setEmail('');  
            setPhone(''); 
        }
        //closing form of adding new contact
        setIsFormOpen(false);
    }

    return (
        <div>
            {!isFormOpen ? (
                //Button for opening form
                <button
                    className="btn btn-outline-secondary mt-5 mb-5"
                    type="button"
                    onClick={() => {setIsFormOpen(true)}}
                >
                    show add contact form
                </button>
            ) : (
                <form>
                    <div className="row">
                        <div className="col-md-1 mb-3">
                            {/*The field for entering the ID of the contact to be added*/}
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Id"
                                value={id}
                                onChange={(event) => {setId(event.target.value)}}
                            />
                        </div>
                        <div className="col-md-3 mb-3">
                            {/*The field for entering the firstName of the contact to be added*/}
                            <input
                                type="text"
                                className="form-control"
                                placeholder="First name"
                                value={firstName}
                                onChange={(event) => {setFirstName(event.target.value)}}
                            />
                        </div>
                        <div className="col-md-3 mb-3">
                            {/*The field for entering the lastName of the contact to be added*/}
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Last name"
                                value={lastName}
                                onChange={(event) => {setLastName(event.target.value)}}
                            />
                        </div>
                        <div className="col-md-2 mb-3">
                            {/*The field for entering the email of the contact to be added*/}
                            <input
                                type="text"
                                className="form-control"
                                placeholder="email"
                                value={email}
                                onChange={(event) => {setEmail(event.target.value)}}
                            />
                        </div>   
                        <div className="col-md-2 mb-3">
                            {/*The field for entering the phone of the contact to be added*/}
                            <input
                                type="text"
                                className="form-control"
                                placeholder="phone"
                                value={phone}
                                onChange={(event) => {setPhone(event.target.value)}}
                            />
                        </div>                        
                    </div>
                    {/*Button for adding a new contact */}
                    <button
                        className='="btn btn-primary'
                        type="button"
                        onClick={() => {submitHandler()}}
                    >
                        add contact
                    </button>
                </form>
            )}
        </div>
    )
}

export default AddContactForm;