import React, { useState } from 'react';

//the form of deleting an existing contact in the table from the table and from the server
const DeleteContactForm = ({
    //the function of removing contact from table and from server
    deleteContactData 
}) => {
    //flag for opening the contact deletion form
    const [isFormOpen, setIsFormOpen] = useState(false);
    //ID of removing contact
    const [id, setId] = useState('');

    //the function, related to button of removing contact according to entered ID
    const submitHandler = () => {
        if (id !== '')
        {
            //if ID is known, then we carring out specified deletion
            deleteContactData(id);
            setId('');
        }
        //in any case, we closing form
        setIsFormOpen(false);
    }

    return (
        <div style={{ margin: "25px 0 0 0"}}>
            {!isFormOpen ? (
                //The button of opening form
                <button
                    className="btn btn-outline-secondary mt-5 mb-5"
                    type="button"
                    onClick={() => {setIsFormOpen(true)}}
                >
                    show delete contact form
                </button>
            ) : (
                <form>
                    <div className="row">
                        <div className="col-md-1 mb-3">
                            {/*The field for entering the ID of the contact to be deleted*/}
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Id"
                                value={id}
                                onChange={(event) => {setId(event.target.value)}}
                            />
                        </div>                      
                    </div>
                    {/*The button of removing contact according to entered ID */}
                    <button
                        className='="btn btn-primary'
                        type="button"
                        onClick={() => {submitHandler()}}
                    >
                        delete contact
                    </button>
                </form>
            )}
        </div>
    )
}

export default DeleteContactForm;