import React, {useState, useEffect} from 'react'
import ArrowUp from '../svg/arrowUp'
import ArrowDown from '../svg/arrowDown'
import SeparateContact from '../separateContact/separateContact';
import SearchElement from '../search/searchElement';

//data table for the current page
const Table = ({
    //collected block of data for current page
    currentBlockRows,
    //all downloaded contacts with server
    contactData,     
    setContactData, 
    //the function of label filtering of loaded contacts
    onSearchSend,    
    //flag request for loading contacts from server
    isRequestOnServer
}) => {
    //the field of soritization
    const [field, setField] = useState('');
    //sorting direction
    const [directionSort, setDirectionSort] = useState(false);

    //flag of showing separate information about any contact in table
    const [rowIsClick, setRowIsClick] = useState(false);
    //separate information about any contact in table
    const [separateContactData, setSeparateContactData] = useState(null);

    //fixation function of separate information about contact, if you clicked on its row in the table
    const separateContact = (contact) => {
        if (contact === separateContactData)
        {
            //clicked again on contact row in table, which is displayed under the table
            setRowIsClick(!rowIsClick);
            //removing separate information about this contact
            setSeparateContactData(null);
        }
        else
        {
            //clicked on row of contact in table
            setRowIsClick(true);
            //fixing separate information about this contact
            setSeparateContactData(contact);
        }
    }

    //the arrow which indicates the sorting direction directionSort === false 
    //sorting goes from bottom to top directionSort === true sorting goes from top to bottom
    const Arrow = () => {
        return (
            directionSort ? <ArrowUp/> : <ArrowDown/> 
        )
    }

    //the function of sorting data according to field field
    const sortData = (field) => {
        const copyData = contactData.concat();
    
        //here will select sorted data
        let sortData;
    
        //sorting from top to bottom
        if (!directionSort) 
        {
            sortData = copyData.sort(
                (a,b) => {
                    return (
                        a[field] > b[field] ? 1 : -1
                    )
                }
            )
        } 

        //sorting from bottom to top
        else 
        {
            sortData = copyData.sort(
                (a,b) => {
                    return (
                        a[field] > b[field] ? -1 : 1
                    )
                }
            )
        }
        
        //fix sorted data
        setContactData(sortData);
        //changing request of next sorted
        setDirectionSort(!directionSort)
    }

    //sorter launch function
    const fieldSortData = (field) => {
        //sorting data by field field
        sortData(field);
        //fixing the sorting field field
        setField(field);
    }

    useEffect(() => {        
        //if a request is made to download new contacts, then we hide
        //all the information about an individual contact, if it was displayed under the table, 
        //hide ee under the table and destroy it
        if (isRequestOnServer) 
        {
            setSeparateContactData(null);
            setRowIsClick(false);
        }
    },
    [
        isRequestOnServer, 
        setSeparateContactData,
        setRowIsClick
    ])

    return (
        <div>
            {/*Block defining of element searching of needed contacts according to all contacts downloaded with server */}
            <SearchElement onSearchSend={onSearchSend}/>
            <table className="table">
                {/*Headering of table */}
                <thead>
                    <tr>
                        <th onClick={() => (fieldSortData('id'))}>
                            id {field === 'id' ? <Arrow/> : null}
                        </th>
                        <th onClick={() => (fieldSortData('FirstName'))}>
                            FirstName {field === 'FirstName' ? <Arrow/> : null}
                        </th>
                        <th onClick={() => (fieldSortData('LastName'))}>
                            LastName {field === 'LastName' ? <Arrow/> : null}
                        </th>
                        <th onClick={() => (fieldSortData('email'))}>
                            email {field === 'email' ? <Arrow/> : null}
                        </th>
                        <th onClick={() => (fieldSortData('phone'))}>
                            phone {field === 'phone' ? <Arrow/> : null}
                        </th>
                    </tr>
                </thead>
                {/*The table itself which is being built from data in set of contacts currentBlockRows*/}
                <tbody>
                    {currentBlockRows.map(
                        (contact) => {
                        return (
                            <tr key={contact.id + contact.email} onClick={() => separateContact(contact)}>
                                <td>{contact.id}</td>
                                <td>{contact.firstName}</td>
                                <td>{contact.lastName}</td>
                                <td>{contact.email}</td>
                                <td>{contact.phone}</td>
                            </tr>
                        )}
                        )}
                </tbody>
            </table>
            {/*Outputing under table separate information about the contact if clicked on his row in table 
               for the first time, if the second time clicked on the same row in the table then close and 
               remove this information */}
            {rowIsClick === true && separateContactData !== null ? <SeparateContact separateContactData={separateContactData}/> : null} 
        </div>
    )
}

export default Table;